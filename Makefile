all: hello

hello: hello.o
	gcc -lm hello.o -o hello 

hello.o: hello.c
	gcc -c hello.c -o hello.o
